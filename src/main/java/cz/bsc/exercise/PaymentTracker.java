package cz.bsc.exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

/**
 * @author Jiri Lorenc (jiri.lorenc@starkysclub.com) on 20.08.2019
 * This class represent all specified amounts of currencies
 */
public class PaymentTracker {

    private final Lock writeLock = new ReentrantReadWriteLock().writeLock();

    private volatile List<CurrencyEntity> currencyEntities = new ArrayList<>();

    public PaymentTracker() {
    }

    public PaymentTracker(List<CurrencyEntity> currencyEntities) {
        this.currencyEntities = currencyEntities;
    }


    public synchronized List<CurrencyEntity> calculateCurrencyEntities() {
        List<CurrencyEntity> result = new ArrayList<>();
        writeLock.lock();
        try {
            for (CurrencyEntity currencyEntity : currencyEntities) {
                if (containsCurrency(result, currencyEntity.getCurrency())) {
                    addAmountOfExistingCurrencyToListOfCurrencies(result, currencyEntity.getCurrency(), currencyEntity.getAmount());
                } else {
                    result.add(new CurrencyEntity(currencyEntity.getCurrency(), currencyEntity.getAmount()));
                }
            }
            result = result.stream().filter(a -> a.getAmount() != 0).collect(Collectors.toList());
            currencyEntities = result;
        } finally {
            writeLock.unlock();
        }
        return result;
    }

    public synchronized void addCurrencyEntity(CurrencyEntity currencyEntity) {
        writeLock.lock();
        try {
            currencyEntities.add(currencyEntity);
        } finally {
            writeLock.unlock();
        }

    }

    @Override
    public String toString() {
        String result = "";
        for (CurrencyEntity currencyEntity : currencyEntities) {
            result += currencyEntity.toString();
            result += System.lineSeparator();
        }
        return result;
    }

    //Helper methods

    /**
     * Return true if currencyEntities contains given currency
     */
    private boolean containsCurrency(List<CurrencyEntity> currencyEntities, String currency) {
        if (currencyEntities.stream().filter(a -> a.getCurrency().equals(currency)).count() > 0) {
            return true;
        } else {
            return false;
        }
    }


    private void addAmountOfExistingCurrencyToListOfCurrencies(List<CurrencyEntity> currencyEntities, String currency, int amount) {
        for (CurrencyEntity currencyEntity : currencyEntities) {
            if (currencyEntity.getCurrency().equals(currency)) {
                currencyEntity.setAmount(currencyEntity.getAmount() + amount);
            }
        }
    }
}
