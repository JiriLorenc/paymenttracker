package cz.bsc.exercise.threads;

import cz.bsc.exercise.CurrencyEntity;
import cz.bsc.exercise.PaymentTracker;
import cz.bsc.exercise.utils.Parser;
import cz.bsc.exercise.utils.LineValidator;

import java.util.Scanner;

/**
 * @author Jiri Lorenc (jiri.lorenc@starkysclub.com) on 21.08.2019
 */
public class HandleInputsThread extends Thread {

    volatile private PaymentTracker paymentTracker;
    private Scanner scanner;
    private Parser parser;
    private LineValidator validator;


    public HandleInputsThread(PaymentTracker paymentTracker) {
        this.paymentTracker = paymentTracker;
        this.scanner = new Scanner(System.in);
        this.parser = new Parser();
        this.validator = new LineValidator();
    }

    @Override
    public void run() {
        while (true) {
            String inputLine = scanner.nextLine();
            if (inputLine.equals("quit")) {
                System.exit(0);
            } else if (validator.validateLine(inputLine)) {
                CurrencyEntity currencyEntity = parser.mapStringToCurrencyEntity(inputLine);
                paymentTracker.addCurrencyEntity(currencyEntity);
            } else {
                System.err.println("Invalid input. Please type input in correct format.");
            }
        }
    }
}
