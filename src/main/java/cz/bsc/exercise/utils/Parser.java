package cz.bsc.exercise.utils;

import cz.bsc.exercise.CurrencyEntity;

/**
 * @author Jiri Lorenc (jiri.lorenc@starkysclub.com) on 20.08.2019
 * Parser for parsing line from command line or file to CurrencyEntity
 */
public class Parser {

    /**
     * Parse string to CurrencyEntity
     */
    public static CurrencyEntity mapStringToCurrencyEntity(String line) {
        String[] splitLine = line.split(" ");
        String currency = splitLine[0];
        int amount = Integer.valueOf(splitLine[1]);
        CurrencyEntity currencyEntity = new CurrencyEntity();
        currencyEntity.setAmount(amount);
        currencyEntity.setCurrency(currency);
        return currencyEntity;
    }
}
