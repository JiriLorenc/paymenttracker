package cz.bsc.exercise;

/**
 * @author Jiri Lorenc (jiri.lorenc@starkysclub.com) on 26.08.2019
 */
public class Constants {

    public static boolean loadFromFile = true;
    public static String pathToFileWithCurrencyEntities = "CurrencyEntities.txt";
}
