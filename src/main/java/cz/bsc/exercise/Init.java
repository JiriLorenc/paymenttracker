package cz.bsc.exercise;

import cz.bsc.exercise.threads.HandleInputsThread;
import cz.bsc.exercise.threads.SumThread;
import cz.bsc.exercise.utils.FileLoader;

import java.io.IOException;
import java.util.List;

/**
 * @author Jiri Lorenc (jiri.lorenc@starkysclub.com) on 21.08.2019
 * This class provides starting and initalizing threads
 */
public class Init {

    private PaymentTracker paymentTracker;
    private SumThread sumThread;
    private HandleInputsThread handleInputsThread;

    private volatile boolean flag = true;

    public Init() {
        if (Constants.loadFromFile) {
            try {
                List<CurrencyEntity> currencyEntities = FileLoader.loadFile(Constants.pathToFileWithCurrencyEntities);
                this.paymentTracker = new PaymentTracker(currencyEntities);
            } catch (IOException e) {
                System.err.println(e);
            }
        } else {
            this.paymentTracker = new PaymentTracker();
        }
        this.sumThread = new SumThread(paymentTracker);
        this.handleInputsThread = new HandleInputsThread(paymentTracker);
    }

    /**
     * This method starts thread for summing and thread for handling input
     */
    public void start() {
        sumThread.start();
        handleInputsThread.start();
    }
}
