package cz.bsc.exercise.utils;

/**
 * @author Jiri Lorenc (jiri.lorenc@starkysclub.com) on 21.08.2019
 * This class provides validation of given line
 */
public class LineValidator {

    private static final String REGEX_PATTERN = "[A-Z]{3} [-]{0,1}[0-9]+";

    /**
     * Return true if line is valid
     */
    public static boolean validateLine(String line) {
        boolean matches = line.matches(REGEX_PATTERN);
        return matches;
    }
}
