package cz.bsc.exercise.threads;

import cz.bsc.exercise.PaymentTracker;

/**
 * @author Jiri Lorenc (jiri.lorenc@starkysclub.com) on 21.08.2019
 */
public class SumThread extends Thread {
    private static final int TIME_OF_CYCLE = 60000;

    private volatile PaymentTracker paymentTracker;

    public SumThread(PaymentTracker paymentTracker) {
        this.paymentTracker = paymentTracker;
    }

    @Override
    public void run() {
        while (true) {
            paymentTracker.calculateCurrencyEntities();
            System.out.println(paymentTracker.toString());
            try {
                Thread.sleep(TIME_OF_CYCLE);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
