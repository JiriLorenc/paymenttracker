package cz.bsc.exercise;

import cz.bsc.exercise.utils.Parser;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * @author Jiri Lorenc (jiri.lorenc@starkysclub.com) on 21.08.2019
 */
public class ParserTest {
    @Test(dataProvider = "mapLineToCurrencyEntityTestDataProvider")
    public void mapLineToCurrencyEntityTest(String testLine, int expectedAmount, String expectedCurrency) {
        Parser parser = new Parser();
        CurrencyEntity currencyEntity = parser.mapStringToCurrencyEntity(testLine);
        assertNotNull(currencyEntity);
        assertEquals(currencyEntity.getAmount(), expectedAmount);
        assertEquals(currencyEntity.getCurrency(), expectedCurrency);
    }

    @DataProvider(name = "mapLineToCurrencyEntityTestDataProvider")
    public Object[][] mapLineToCurrencyEntityTestDataProvider() {
        return new Object[][]{
                { "USD 30", 30, "USD"},
                { "CZK 0", 0, "CZK"},
                { "EUR -1", -1, "EUR"}
        };
    }
}
