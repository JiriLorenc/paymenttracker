package cz.bsc.exercise;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jiri Lorenc (jiri.lorenc@starkysclub.com) on 21.08.2019
 */
public class PaymentTrackerTest {

    @Test(dataProvider = "resolveTestDataProvider")
    public void resolveTest(List<CurrencyEntity> currencyEntities, int expectedAmountCZK, int expectedAmountUSD, int expectedSize) {
        PaymentTracker paymentTracker = new PaymentTracker();
        for (CurrencyEntity currencyEntity : currencyEntities) {
            paymentTracker.addCurrencyEntity(currencyEntity);
        }
        List<CurrencyEntity> result = paymentTracker.calculateCurrencyEntities();
        Assert.assertEquals(result.size(), expectedSize);
        Assert.assertEquals(result.get(0).getAmount(), expectedAmountCZK);
        Assert.assertEquals(result.get(1).getAmount(), expectedAmountUSD);
    }


    @DataProvider(name = "resolveTestDataProvider")
    public Object[][] resolveTestDataProvider() {
        List<CurrencyEntity> currencyEntities = new ArrayList<>();
        CurrencyEntity czk = new CurrencyEntity("CZK", 10);
        CurrencyEntity usd = new CurrencyEntity("USD", 10);
        CurrencyEntity jpn = new CurrencyEntity("JPN", 0);
        currencyEntities.add(czk);
        currencyEntities.add(czk);
        currencyEntities.add(usd);
        currencyEntities.add(jpn);

        return new Object[][]{
                {currencyEntities,
                        20,
                        10,
                        2
                }
        };
    }

}