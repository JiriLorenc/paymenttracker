package cz.bsc.exercise;

/**
 * @author Jiri Lorenc (jiri.lorenc@starkysclub.com) on 20.08.2019
 * Main class for starting program
 */
public class Main {


    /**
     * Main method this program
     */
    public static void main(String[] args) {
        Init init = new Init();
        init.start();
    }
}
