package cz.bsc.exercise;

/**
 * @author Jiri Lorenc (jiri.lorenc@starkysclub.com) on 20.08.2019
 * This class represented one line typed in command line or laoded from file
 */
public class CurrencyEntity {

    private String currency;

    private int amount;

    public CurrencyEntity() {
    }

    public CurrencyEntity(String currency, int amount) {
        this.currency = currency;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return currency + " "+ amount ;
    }

    //getters and setters
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
