package cz.bsc.exercise.utils;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import cz.bsc.exercise.CurrencyEntity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jiri Lorenc (jiri.lorenc@starkysclub.com) on 21.08.2019
 *  This class provides loading CurrencyEntities from file
 */
public class FileLoader {


    /**
     * This method loads CurrencyEntities from file
     */
    public static List<CurrencyEntity> loadFile(String path) throws IOException {

        ClassLoader classLoader = FileLoader.class.getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(path);

        List<CurrencyEntity> currencyEntities = new ArrayList<>();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (LineValidator.validateLine(line)) {
                    CurrencyEntity currencyEntity = Parser.mapStringToCurrencyEntity(line);
                    currencyEntities.add(currencyEntity);
                    System.out.println(currencyEntity);
                } else {
                    throw new IOException("Loading CurrencyEntities from file failed.");
                }
            }
        }
        return currencyEntities;
    }
}