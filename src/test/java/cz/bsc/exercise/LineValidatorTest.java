package cz.bsc.exercise;

import cz.bsc.exercise.utils.LineValidator;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * @author Jiri Lorenc (jiri.lorenc@starkysclub.com) on 21.08.2019
 */
public class LineValidatorTest {

    @Test(dataProvider = "validateLineTestDataProvider")
    public void validateLineTest(String testLine) {
        LineValidator lineValidator = new LineValidator();
        boolean isValid = lineValidator.validateLine(testLine);
        assertTrue(isValid);
    }

    @DataProvider(name = "validateLineTestDataProvider")
    public Object[][] validateLineTestDataProvider() {
        return new Object[][]{
                {"USD 30"},
                {"USD -30"},
                {"USD 0"}
        };
    }

    @Test(dataProvider = "validateIncorrectLineTestDataProvider")
    public void validateIncorrectLineTest(String testLine) {
        LineValidator validator = new LineValidator();
        boolean isValid = validator.validateLine(testLine);
        assertFalse(isValid);
    }

    @DataProvider(name = "validateIncorrectLineTestDataProvider")
    public Object[][] validateIncorrectLineTestDataProvider() {
        return new Object[][]{
                {" USD 30"},
                {"USD -30k "},
                {"USD 0 "},
                {""},
                {"usd 40"},
                {"30 USD"}
        };
    }
}